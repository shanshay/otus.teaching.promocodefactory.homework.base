using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task CreateAsynk(T employee)
        {
            if (Data.FirstOrDefault(x => x.Id == employee.Id) == null)
            {
                return Task.FromResult(Data = Data.Append(employee).ToList());
            }
            else
            {
                throw new ArgumentException();
            }

        }

        public Task UpdateAsynk(T employee)
        {
            if (Data.FirstOrDefault(x => x.Id == employee.Id) != null)
            {
                Data = Data.Where(x => x.Id != employee.Id).ToList();
                Data = Data.Append(employee).ToList();
                return Task.CompletedTask;
            }
            else
            {
                throw new ArgumentException();
            }

        }

        public Task DeleteAsynk(T employee)
        {
            return Task.FromResult(Data = Data.Where(x => x.Id != employee.Id).ToList());
        }

    }
}
